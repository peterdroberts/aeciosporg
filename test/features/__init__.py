from aloe import world, step
import aeciosporg
import os
import re


@step(r"\"([^\s]+)\" as the file")
def _get_the_file(self, filename):
    world.filename = filename
    world.filepath = os.path.join(os.path.dirname(__file__), filename)


@step(r"parse the file")
def _parse_filename_step(self):
    world.out, world.lines = aeciosporg.Org(world.filepath).parse()


@step(r"read back line #(\d+)")
def _read_line(self, lineno):
    world.line = world.lines[int(lineno)]


@step(r"interpret the line as type ([^\s]+)")
def _interpret_line(self, linetype):
    assert type(world.line).__name__.casefold() == linetype.casefold()


@step(r"the line should have parent ([^\s]+)")
def _get_parent(self, parent):
    parent = re.sub(r'^(line |#)', '', parent)
    try:
        lineno = int(parent)
    except:
        lineno = None
    if lineno is None:
        assert parent.casefold() == str(world.line.parent).casefold()
    else:
        assert world.lines[lineno] == world.line.parent


@step(r"the metadata should be set as so")
def _get_metadata(self):
    for line in self.hashes:
        assert getattr(world.out, line['metadata'].casefold()) == line['value']


@step(r"the line count should be (\d+)")
def _get_linecount(self, linecount):
    assert len(world.lines) == int(linecount)
