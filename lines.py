"""File containing definitions of nodes by line"""
import re
import unicodedata
from copy import copy
import logging as log
import abc

import commands
import custom

FLAGS = re.I + re.U
TABWIDTH = 3

encoding = 'utf-8'
defvars = ('_regex', 'org', 'content', 'parent', 'children')

priority = {
    'Cmd': 10,
    'Block': 4,
    'Txt': -1,
}


class Line:
    """Stores basic info about node. Not part of a structure"""
    __slots__ = defvars

    def __init__(self, org, content):
        self.org = org
        self.content = content.rstrip()

    def __str__(self):
        """output string representation of node"""
        return self.component['name']

    def __index__(self):
        return self.org.nodes[self]['line']

    def __bytes__(self):
        return self.content

    def __iter__(self):
        yield self

    def do(self):
        """Do something after this line is parsed"""
        pass

    @property
    def typename(self):
        """Name of type such that it can be used in format
        expressions"""
        return type(self).__name__

    @property
    def indent(self):
        try:
            indent = (self.component['indent'].count(" ") +
                      self.component['indent'].count("\t") * TABWIDTH)

            return indent
        except:
            return None

    @property
    def component(self):
        """Regular expression object"""
        return re.match(self._regex, self.content, flags=FLAGS).groupdict("")

    @property
    def weighting(self):
        return self.indent * -1 if self.indent is not None else 0


class Cmd(Line):
    """Parses commands.
    Commands are in the form #+COMMAND: arguments"""

    _regex = r"(?P<prexix>#\+)(?!begin)(?P<name>\w+):(?P<cmd>.*)"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        cmd = None if self.component['cmd'] is "" else self.component['cmd']
        try:
            getattr(commands, self.component['name'].lower())(self.org, cmd)
        except:
            try:
                getattr(custom.commands, self.component['name'].lower())(
                    self.org, cmd)
            except:
                pass
        else:
            log.info("Skipped {} command. No definition found".format(
                self.component['name']))


class Block(Line):
    _regex = r"(?P<prefix>#\+begin_)(?P<type>[a-z]+)(:?\s*(?P<args>.*))?"


class EndBlock(Line):
    _regex = r"(?P<prefix>#\+end_)(?P<type>[a-z]+)"


class Field(Line):
    _regex = r"(?P<prefix>:)(?P<name>\w+):\s+(?P<value>.*)"


class Scheduled(Line):
    _regex = r"(?P<prefix>scheduled:)\s*(?P<value>.*)"


class Deadline(Line):
    _regex = r"(?P<prefix>deadline:)\s*(?P<value>.*)"


class Closed(Line):
    _regex = r"(?P<prefix>deadline:)\s*(?P<value>.*)"


class Drawer(Line):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    _regex = r"(?P<prefix>:)(?!end:)(?P<name>\w+)(?P<postfix>:)$"


class EndDrawer(Line):
    _regex = r"(:end:)\s*$"


class Heading(Line):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._regex = (r"(?P<prefix>\*+)\s+" + r"((?P<todo>" +
                       r"|".join(self.org.todo_keywords) + r")|" + r"(?P<done>"
                       + r"|".join(self.org.done_keywords) + r")\s+)?" +
                       r"(\[#(?P<priority>[a-z0-9])\]\s*)?" +
                       r"(?P<name>.*?)(\s*(?P<tags>:(\w+:)+))?$")

    _regex = r"(?P<prefix>\*+)\s"

    @property
    def tags(self):
        if self.component['tags'] is not "":
            return set(retags.group(0)[1:-1].split(":"))
        else:
            return None

    @property
    def depth(self):
        return re.match(self._regex, self.content).group('prefix').count("*")

    @property
    def weighting(self):
        return 32 - (self.depth + 1) * 1

    @property
    def todokeyword(self):
        return self.component['todo'].upper() if self.component[
            'todo'] is not "" else None

    @property
    def donekeyword(self):
        return self.component['done'].upper() if self.component[
            'done'] is not "" else None

    @property
    def done(self):
        return True if self.component['done'] is not "" else False

    @property
    def todo(self):
        return True if self.component['todo'] is not "" else False


class Blank(Line):
    _regex = r"([\s ]*(?=$))"

    def __add__(self, other):
        raise TypeError


class ListItem(Line):
    _regex = (
        r"^(?P<indent>\s*)(?P<prefix>([-+]|[0-9]+[\.\)]))\s+" +
        r"(\[(?P<radio>[x ])\]\s+)?" +
        r"(((?P<term>.*?)\s+::\s+(?P<definition>.*))|" + r"(?P<text>.*)\s*)$")

    def __add__(self, other):
        if 'prefix' not in other.component and 'indent' in other.component:
            if other.component['indent'] - self.component['indent'] > 2:
                self.content = " ".join([self.content, other.content.strip()])
                return self
        else:
            raise TypeError

    def __bool__(self):
        if self.component['radio'] is not "":
            return True if self.component['radio'].lower() == 'x' else False


class Comment(Line):
    _regex = r"(?P<indent>\s*)(?P<prefix># )(?P<text>.*)"


class Verbose(Line):
    _regex = r"(?P<indent>\s*)(?P<prefix>: )(?P<text>.*)"


class Txt(Line):
    _regex = r"(?P<indent>\s*)(?P<text>.*)"

    def __add__(self, other):
        self.content = " ".join([self.content, other.content])
        return self

    def __init__(self, org, *args, **kwargs):
        super().__init__(org, *args, **kwargs)

    def __str__(self):
        return self.component['text']
