Repository for Peter Roberts' version of org-mode

# Files
## aeciosporg.py
Main file for running parser/co-ordinating different elements of parser.

## lines.py
Definitions of nodes and how to parse them.

## commands.py
Commands to carry out in response to `#+COMMAND` style command lines.

