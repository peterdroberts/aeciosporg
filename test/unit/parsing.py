import utils
import aeciosporg


class parsing(utils.orgtest):
    def testGeneralMetaData(self):
        """MetaData is extracted from file"""
        general = """#+TITLE: Org-mode test
#+AUTHOR: Peter Roberts
#+EMAIL: peter.roberts@gmail.com"""
        out, lines = aeciosporg.Org(general, True).parse()
        self.assertEqual(out.title, "Org-mode test")
        self.assertEqual(out.author, "Peter Roberts")
        self.assertEqual(out.email, "peter.roberts@gmail.com")

    def testParagraph(self):
        """Paragraph text is concatenated"""
        paragraph = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at
quam vel purus vulputate dapibus. Curabitur consequat nisi sed turpis
efficitur, sed placerat velit lobortis. Phasellus viverra, quam eget
facilisis lobortis, dolor diam mattis quam, id sodales nibh massa vel
arcu. Etiam mattis dictum nibh, eu posuere felis maximus aliquam. Cras
neque elit, mollis sit amet vulputate eget, posuere et dui. Vivamus ac
diam vitae massa viverra malesuada. Phasellus cursus egestas magna, ac
venenatis urna feugiat eu. In porttitor, eros at scelerisque tempor,
nunc leo iaculis arcu, at consectetur enim tellus vel ante. Phasellus
rhoncus ex ac eros posuere suscipit. Aliquam eget ullamcorper sapien,
eu mattis mi. Suspendisse posuere eleifend orci sit amet
sodales. Vestibulum a commodo ipsum. Nunc elementum suscipit
dapibus. Pellentesque ac odio enim. Suspendisse ornare, mi at vehicula
consequat, leo ante luctus tortor, eu mollis nulla erat a risus."""

        desired = paragraph.replace('\n', ' ').replace('\r', '')
        out, lines = aeciosporg.Org(paragraph, True).parse()
        self.assertEqual(str(lines[0]), desired)
        self.assertEqual(len(lines), 1)

    def testStripPrefix(self):
        """Prefix is split from body text"""
        testtext = """* Test
** Test Level 2
- Test
  - Test"""

        out, lines = aeciosporg.Org(testtext, True).parse()
        for i in lines:
            try:
                a = i.component['text']
            except:
                a = i.component['name']
            self.assertTrue(a.startswith('Test'))

    def testAFile(self):
        """A quick test on a file to see how it is processed"""
        for line in aeciosporg.Org("/home/peter/Dropbox/org/home.org_archive"):
            print("{!r}".format(line.component))

    def testLevels(self):
        """Levels are recognised correctly"""
        testtext = """* Test
** Test Level 2
- Test
  - Test"""

        parent = None
        elements = []
        for i in aeciosporg.Org(testtext, True):
            self.assertTrue(i.parent == parent,
                            "Parents not assigned correctly")
            parent = i
            elements.append(i)
        self.assertEqual(
            len(elements), 4, "Incorrect number of elements generated")

    def testComplexLevels(self):
        """Differing indents yield correct hierarchy"""
        testtext = """* Test Level 1
** Test Level 2
** Test Level 2
- Test
* Test Level 1
  - Test"""

        elements = []
        for i in aeciosporg.Org(testtext, True):
            elements.append(i)
        self.assertTrue(elements[0].parent is None,
                        "First element has incorrect parent")
        self.assertTrue(elements[1].parent is elements[0],
                        "Second element has incorrect parent")
        self.assertTrue(elements[2].parent is elements[0],
                        "Third element has incorrect parent")
        self.assertTrue(elements[3].parent is elements[2],
                        "Fourth element has incorrect parent")
        self.assertTrue(elements[4].parent is None,
                        "Fifth element has incorrect parent")
        self.assertTrue(elements[5].parent is elements[4],
                        "Sixth element has incorrect parent")
