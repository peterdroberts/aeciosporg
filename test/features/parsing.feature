Feature: Parsing
  The parser should recognise the different types of lines
  
  Scenario Outline: Parsing a line
    Given "<filename>" as the file
    When I parse the file
    And I read back line #<lineno>
    Then the parser should interpret the line as type <linetype>
    And the line should have parent <parent>

    Examples:
      | filename         | lineno | linetype | parent |
      | generalmeta.org  |      0 | cmd      | none   |
      | paragraph.org    |      0 | txt      | none   |
      | hierarchical.org |      0 | heading  | none   |
      | hierarchical.org |      3 | heading  | #0     |
      | hierarchical.org |      4 | listitem | #3     |
      | hierarchical.org |      6 | listitem | #5     |

  Scenario: Parsing metadata
    Given "generalmeta.org" as the file
    When I parse the file
    Then the metadata should be set as so:
      | metadata | value                   |
      | title    | Org-mode test           |
      | author   | Peter Roberts           |
      | email    | peter.roberts@gmail.com |

  Scenario Outline: Parsing a file
    Given "<filename>" as the file
    When I parse the file
    Then the line count should be <linecount>

    Examples:
      | filename         | linecount |
      | generalmeta.org  |         3 |
      | paragraph.org    |         1 |
      | hierarchical.org |         7 |
