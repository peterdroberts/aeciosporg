import re


def todo(org, cmd):
    cmd = cmd.strip()
    splitargs = cmd.split("|")

    if len(splitargs) == 2:
        org.done_keywords.update(splitargs[1].split(" "))
    org.todo_keywords.update(splitargs[0].split(" "))


def title(org, cmd):
    title = cmd.strip()
    org.title = title


def author(org, cmd):
    author = cmd.strip()
    org.author = author


def email(org, cmd):
    email = cmd.strip()
    org.email = email
