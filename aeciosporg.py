import re
import unicodedata
from inspect import isclass, getmembers
from operator import itemgetter

import lines
from orderedset import OrderedSet
import custom

encoding = 'utf-8'
FLAGS = re.I | re.U
TABWIDTH = 3
TODO_KEYWORDS = {'TODO', }
DONE_KEYWORDS = {'DONE', }


def classfilter(x):
    return True if isclass(x) and x is not lines.Line else False


class Org:
    """Parses org file"""

    def __init__(self, src, fromstring=False):
        a = [(v, lines.priority.get(i, 0))
             for i, v in getmembers(lines, classfilter)]
        b = [(v, custom.lines.priority.get(i, 0))
             for i, v in getmembers(custom.lines, classfilter)]
        self.lineclasses = a + b
        self.src = src
        self.todo_keywords = TODO_KEYWORDS
        self.done_keywords = DONE_KEYWORDS
        self.fromstring = fromstring
        self.context = self.defaultcontext
        self.buffer = None
        super().__init__()

    def __enter__(self):
        if self.fromstring:
            string = self.src.replace('\r\n', '\n').replace('\r', '\n')
            string = string[:-1] if string[-1] == '\n' else string
            string = unicodedata.normalize('NFC', string)
            self._orgfile = string.split("\n")
        else:
            self._orgfile = open(self.src, 'r')
        return self._orgfile

    def __exit__(self, exc_type, exc_value, traceback):
        if not self.fromstring:
            self.orgfile.close()

    @property
    def orgfile(self):
        return self._orgfile

    @property
    def orderedset(self):
        return OrderedSet([i for i in self])

    def parse(self):
        orderedset = self.orderedset
        return self, orderedset

    def __iter__(self, evaluate=True):
        with self as orgfile:
            for string in orgfile:
                string = self.normalize(string)
                for out in self.context(string):
                    if (isinstance(out, lines.Blank)):
                        if self.buffer is not None:
                            yield self.buffer
                            self.buffer = None
                        continue
                    try:
                        # Type spans multiple lines (can be concatenated)
                        self.buffer += out
                    except TypeError:
                        # Cannot concatenate types
                        if self.buffer is not None:
                            yield self.buffer
                            out.parent = self.buffer
                            self.buffer = None
                            while (out.parent is not None and
                                   out.parent.weighting <= out.weighting):
                                out.parent = out.parent.parent
                        else:
                            out.parent = None
                        self.buffer = out
            if self.buffer is not None:
                yield self.buffer
                self.buffer = None

    def normalize(self, string):
        string = string.replace('\r\n', '\n').replace('\r', '\n')
        try:
            string = string[:-1] if string[-1] == '\n' else string
        except:
            pass
        return unicodedata.normalize('NFC', string)

    def defaultcontext(self, string):
        string = self.normalize(string)
        for lineclass, priority in sorted(
                self.lineclasses, key=itemgetter(1), reverse=True):
            match = re.match(lineclass._regex, string, flags=FLAGS)
            if match is not None:
                break
        yield from lineclass(self, string)
